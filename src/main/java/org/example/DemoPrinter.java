package org.example;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.CharBuffer;

public class DemoPrinter {

    public void print() {
        try (var reader = new InputStreamReader(getClass().getResourceAsStream("/demo.txt"))) {
            var buffer = CharBuffer.allocate(256);
            while (reader.read(buffer) >= 0) {
                System.out.append(buffer.flip());
                buffer.clear();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
