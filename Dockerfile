FROM docker.io/library/maven:3.9-eclipse-temurin-17 as maven
WORKDIR /app
COPY pom.xml ./
RUN mvn dependency:go-offline
COPY src/ ./src
RUN mvn clean package && \
    (cd target && ln -s graalvm-demo-*.jar app.jar)

FROM ghcr.io/graalvm/graalvm-community:22 as graalvm
WORKDIR /app
COPY --from=maven /app/target/app.jar ./
RUN native-image -jar app.jar

FROM docker.io/library/alpine:3.19
RUN apk add --no-cache gcompat && \
    adduser --system app
USER app
WORKDIR /app
COPY --from=graalvm /app/app ./
CMD ./app
